const {PasswordHash} = require('node-phpass');
const bcrypt = require('bcryptjs');

const len = 8;
const portable = true;
const phpversion = 7;

function hash(password) {
    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(password, salt);

    return hash;
}

function check(password, storedHash) {
    let ret = false;
    const hasher = new PasswordHash(len, portable, phpversion);

    // bcrypt
    if (storedHash.substring(0, 2) === '$2') {
        ret = bcrypt.compareSync(password, storedHash);
    // phpass
    } else if (storedHash.substring(0, 3) === '$P$') {
        ret = hasher.CheckPassword(password, storedHash);
    }

    return ret;
}

module.exports.hash = hash;
module.exports.check = check;

