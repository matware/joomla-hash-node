const hasher = require('./lib/hash.js');

const password = '123456';
const bcryptHash = '$2y$10$FZxS6C14KVCZhskklnFVx.FPd0CdIx8cb.aif2Vecz/FaLxM4qsgi';
const phpassHash = '$P$BzHMWwAio4pPqp1wuwdc18X8HoFIrd1';
const wrongPassword = '$P$B6o85rwWL8eNMkSeQCRvgEziznOScd.';

// Encode password
var hash = hasher.hash(password);
console.log("Joomla! hash for password: " + password + " is: " + hash)

// Compare bcrypt
var checkBcrypt = hasher.check(password, bcryptHash);
console.log("Bcrypt password checking results: " + checkBcrypt);

// Compare Phpass
var checkPhpass = hasher.check(password, phpassHash);
console.log("PHPass password checking results: " + checkPhpass);

// Compare wrong password
var wrongCheck = hasher.check(password, wrongPassword);
console.log("Wrong password checking results: " + wrongCheck);
